<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministrationBundle\Mocks;

use RuntimeException;
use SplFileInfo;
use Talentry\Slack\Channel;
use Talentry\Slack\File;
use Talentry\Slack\Message;
use Talentry\Slack\SlackClient;

class MockSlackClient extends SlackClient
{
    public function __construct()
    {
    }

    public function send(Message $message, ?Channel $channel = null): void
    {
        $this->throwException();
    }

    public function sendPlainTextMessage(string $message, ?Channel $channel = null): void
    {
        $this->throwException();
    }

    public function sendMarkdownMessage(string $message, ?Channel $channel = null): void
    {
        $this->throwException();
    }

    public function getChannel(string $name): ?Channel
    {
        $this->throwException();
    }

    public function getDefaultChannel(): Channel
    {
        $this->throwException();
    }

    public function uploadFile(SplFileInfo $file, ?string $title = null): File
    {
        $this->throwException();
    }

    private function throwException(): void
    {
        throw new RuntimeException(
            'In order to use slack integration, you need to install and configure talentrydev/slack-bundle'
        );
    }
}
