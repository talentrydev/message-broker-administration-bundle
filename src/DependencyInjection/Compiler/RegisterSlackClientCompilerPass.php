<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministrationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Talentry\MessageBrokerAdministrationBundle\Mocks\MockSlackClient;
use Talentry\Slack\SlackClient;

class RegisterSlackClientCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition(SlackClient::class)) {
            $container->setDefinition(SlackClient::class, new Definition(MockSlackClient::class));
        }
    }
}
