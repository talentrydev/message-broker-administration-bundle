<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministrationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Talentry\MessageBrokerAdministration\Infrastructure\MessageRequeuer\CompositeMessageRequeuer;

class RegisterMessageRequeuersCompilerPass implements CompilerPassInterface
{
    public const string TAG_MESSAGE_BROKER_MESSAGE_REQUEUER = 'messageBroker.messageRequeuer';

    public function process(ContainerBuilder $container): void
    {
        $compositeRequeuer = $container->findDefinition(CompositeMessageRequeuer::class);

        foreach ($container->findTaggedServiceIds(self::TAG_MESSAGE_BROKER_MESSAGE_REQUEUER) as $id => $tags) {
            $serviceDefinition = $container->getDefinition($id);
            //don't register composite message requeuer to itself
            if ($serviceDefinition->getClass() === CompositeMessageRequeuer::class) {
                continue;
            }

            $compositeRequeuer->addMethodCall('registerRequeuer', [new Reference($id)]);
        }
    }
}
