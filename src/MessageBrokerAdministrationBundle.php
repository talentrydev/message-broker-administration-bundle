<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministrationBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Talentry\MessageBrokerAdministrationBundle\DependencyInjection\Compiler\RegisterMessageRequeuersCompilerPass;
use Talentry\MessageBrokerAdministrationBundle\DependencyInjection\Compiler\RegisterSlackClientCompilerPass;

class MessageBrokerAdministrationBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new RegisterMessageRequeuersCompilerPass());
        $container->addCompilerPass(new RegisterSlackClientCompilerPass());
    }
}
