<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministrationBundle\Tests;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use JMS\SerializerBundle\JMSSerializerBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;
use Talentry\MessageBrokerAdministrationBundle\MessageBrokerAdministrationBundle;
use Talentry\MessageBrokerBundle\MessageBrokerBundle;

class TestKernel extends Kernel
{
    public function registerBundles(): iterable
    {
        return [
            new FrameworkBundle(),
            new MonologBundle(),
            new MessageBrokerBundle(),
            new MessageBrokerAdministrationBundle(),
            new DoctrineBundle(),
            new JMSSerializerBundle(),
        ];
    }

    public function getProjectDir(): string
    {
        return dirname(__DIR__);
    }

    public function getRootDir(): string
    {
        return $this->getProjectDir();
    }

    public function getCacheDir(): string
    {
        return dirname(__DIR__) . '/var/cache/' . $this->getEnvironment();
    }

    public function getLogDir(): string
    {
        return dirname(__DIR__) . '/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load(__DIR__ . sprintf('/config/config_%s.yml', $this->getEnvironment()));
    }
}
