<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministrationBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Routing\RouterInterface;

class RoutingTest extends KernelTestCase
{
    private RouterInterface $router;

    protected function setUp(): void
    {
        $this->router = self::getContainer()->get(RouterInterface::class);
    }

    public function testRouting(): void
    {
        $routes = $this->router->getRouteCollection()->all();
        self::assertNotEmpty($routes);
        foreach ($routes as $route) {
            self::assertStringStartsWith('/api/v1/admin/message-broker/', $route->getPath());
        }
    }
}
