# Message Broker Administration Bundle

This is a Symfony bundle used for integrating talentrydev/message-broker-administration library into a Symfony project.
It is an add-on bundle, so you cannot use it without talentrydev/message-broker-bundle.

## Installing

* Run:

```
composer require talentrydev/message-broker-administration-bundle
```

* Add the MessageBrokerAdministrationBundle to your kernel's `registerBundles` method:

```
return [
    //...
    new \Talentry\MessageBrokerAdministrationBundle\MessageBrokerAdministrationBundle();
];
```

* To load doctrine mappings from annotations, add the following to your doctrine config:

```
doctrine:
    orm:
        entity_managers:
            default:
                mappings:
                    MessageBrokerAdministration:
                        type: attribute
                        dir: '%kernel.project_dir%/vendor/talentrydev/message-broker-administration/src/Domain/Entity'
                        prefix: 'Talentry\MessageBrokerAdministration\Domain\Entity'
```

* To execute doctrine migrations, add the following to your doctrine-migrations bundle config:

```
doctrine_migrations:
  migrations_paths:
    'Talentry\MessageBrokerAdministration\Infrastructure\DoctrineMigrations': '%kernel.project_dir%/vendor/talentrydev/message-broker-administration/src/Infrastructure/DoctrineMigrations'
```

* To register bundle routes, add the following to the symfony routing file (prefix is optional and can be set to anything you choose):

```
message_broker_administration:
  resource: '@MessageBrokerAdministrationBundle/Resources/config/routing.yml'
  prefix: '/api/v1/admin/message-broker/'
```